import json

from entities import Graph, Relation


def parse():
    graph = Graph()
    with open("assets.json") as file:
        for line in file.readlines():
            el = json.loads(line)
            process(graph, el)

    return graph


def process(graph, el):
    name = "/".join(el.get('name').split('/')[-2:])
    type = el.get('asset_type').split('/')[-1]
    from_entity = graph.get_or_create_entity(name=name)
    from_entity.set_type(type_=type)

    for binding in el.get('iam_policy', {}).get('bindings'):
        role = binding.get('role')
        for member in binding.get('members'):
            to_entity = graph.get_or_create_entity(name=member)
            to_entity.set_type('member')
            relation = Relation(type_='binding', from_=from_entity, to_=to_entity, role=role)
            graph.add_relation(relation)

    start = from_entity
    for ancestor in el.get('ancestors'):
        to_entity = graph.get_or_create_entity(name=ancestor)
        if start == to_entity:
            continue
        relation = Relation(type_='ancestor', from_=start, to_=to_entity)
        graph.add_relation(relation)
        start = to_entity
