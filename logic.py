from typing import List, Generator
from entities import Graph, Entity, Relation


def get_ancestor(graph: Graph, entity: Entity) -> Entity:
    relations = graph.get_to_relations('ancestor', entity)
    if relations:
        return relations[0].to_


def get_children(graph: Graph, entity: Entity) -> Generator[Entity, None, None]:
    relations = graph.get_from_relation('ancestor', entity)
    for relation in relations:
        yield relation.from_
        yield from get_children(graph, relation.from_)


def get_bindings(graph: Graph, entity: Entity) -> List[Relation]:
    return graph.get_from_relation('binding', entity)


# ------2nd task -------
def get_hierarchy(graph: Graph, name: str):
    result = []
    entity = graph.get_entity(name)
    while True:
        ancestor = get_ancestor(graph, entity)
        if ancestor is None:
            break
        result.append(ancestor)
        entity = ancestor

    return result


# -------3rd task -------
def get_permissions(graph: Graph, name: str):
    result = set()
    entity = graph.get_entity(name)

    for relation in get_bindings(graph, entity):
        role = relation.properties.get('role')
        entity = relation.from_
        result.add((entity.name, entity.type, role))

        for child in get_children(graph, entity):
            result.add((child.name, child.type, role))

    return list(result)
