## Permission Graph 
Project is implementing graph and functions:

1. Task 2 - Resource hierarchy

2. Task 3 - Who has what


#### Project setup:

`cd graph_test`

`virtualenv venv -p python3`


#### Run tests:

`python tests.py`


#### Run project:

`python main.py`

