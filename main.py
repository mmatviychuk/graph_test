from logic import get_hierarchy, get_permissions
from parser import parse


def main():
    graph = parse()
    graph.print()

    result = get_hierarchy(graph, "folders/767216091627")
    print(result)
    # [<Organization:organizations/1066060271767>]

    permissions = get_permissions(graph, "projectOwner:intrepid-fiber-264210")
    print(permissions)
    # [('storage.googleapis.com/authomize-exercise-data', 'Bucket', 'roles/storage.legacyBucketOwner')]


if __name__ == "__main__":
    main()
