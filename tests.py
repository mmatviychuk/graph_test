import unittest

from logic import get_hierarchy, get_permissions
from parser import parse


class TestGraph(unittest.TestCase):
    graph = parse()

    def test_get_hierarchy1(self):
        result = get_hierarchy(self.graph, "folders/767216091627")
        names = [el.name for el in result]
        self.assertEqual(names, ["organizations/1066060271767"])

    def test_get_hierarchy2(self):
        result = get_hierarchy(self.graph, "folders/635215680011")
        names = [el.name for el in result]
        self.assertEqual(names, ["folders/767216091627","organizations/1066060271767"])

    def test_get_hierarchy3(self):
        result = get_hierarchy(self.graph, "folders/361332156337")
        names = [el.name for el in result]
        self.assertEqual(names, ["folders/96505015065","folders/767216091627","organizations/1066060271767"])

    def test_get_permissions1(self):
        result = get_permissions(self.graph, "projectOwner:intrepid-fiber-264210")
        self.assertEqual(result, [('storage.googleapis.com/authomize-exercise-data', 'Bucket', 'roles/storage.legacyBucketOwner')])

    def test_get_permissions2(self):
        result = get_permissions(self.graph, "group:reviewers@test.authomize.com")
        self.assertEqual(set(result), set([('folders/361332156337', 'Folder', 'roles/viewer'),
                                           ('folders/93198982071', 'Folder', 'roles/viewer'),
                                           ('folders/96505015065', 'Folder', 'roles/viewer')]))



if __name__ == "__main__":
    unittest.main()
