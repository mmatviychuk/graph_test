from typing import List
from collections import defaultdict


class Entity:
    type: str = None  # user, folder, project etc
    name: str
    properties: dict

    def __init__(self, name: str):
        self.name = name

    def set_type(self, type_: str):
        assert self.type is None or self.type == type_, 'Entity type inconsistency'
        self.type = type_

    def __repr__(self) -> str:
        return f'<{self.type}:{self.name}>'


class Relation:
    type: str  # ancestor, binding
    from_: Entity
    to_: Entity
    properties: dict  # role

    def __init__(self, type_: str, from_: Entity, to_: Entity, **properties):
        self.type = type_
        self.from_ = from_
        self.to_ = to_
        self.properties = properties

    def __repr__(self):
        return f'{self.from_} -[{self.type}]-> {self.to_}'


class Graph:
    relations: List[Relation]
    entities: List[Entity]

    def __init__(self):
        self.relations = []
        self.entities = []
        self._index_to = defaultdict(list)
        self._index_from = defaultdict(list)

    def get_entity(self, name: str) -> Entity:
        for entity in self.entities:
            if entity.name == name:
                return entity

    def get_or_create_entity(self, name: str) -> Entity:
        entity = self.get_entity(name)
        if entity is None:
            entity = Entity(name=name)
            self.entities.append(entity)
        return entity

    def add_relation(self, relation: Relation):
        self._index_to[(relation.type, relation.to_)].append(relation)
        self._index_from[(relation.type, relation.from_)].append(relation)
        self.relations.append(relation)

    def print(self):
        for entity in self.entities:
            print(entity)
        for relation in self.relations:
            print(relation)

    def get_to_relations(self, type_: str, entity: Entity) -> List[Relation]:
        return self._index_from[(type_, entity)]

    def get_from_relation(self, type_: str, entity: Entity) -> List[Relation]:
        return self._index_to[(type_, entity)]
